#! /bin/bash
set -e


# SOFTWARE VERSIONS
LOFARSTMAN_COMMIT=33aca20bac7bab12759b7b2dec463bddb84b60f3
DYSCO_COMMIT=master
AOFLAGGER_COMMIT=master
EVERYBEAM_COMMIT=v0.6.0
DP3_COMMIT=master

SCRIPT_PATH=$(realpath ${BASH_SOURCE[0]})

DOCKER_PATH=$(dirname ${SCRIPT_PATH})
docker build --pull ${DOCKER_PATH} \
                    --build-arg LOFARSTMAN_COMMIT=${LOFARSTMAN_COMMIT} \
                    --build-arg DYSCO_COMMIT=${DYSCO_COMMIT} \
                    --build-arg AOFLAGGER_COMMIT=${AOFLAGGER_COMMIT} \
                    --build-arg EVERYBEAM_COMMIT=${EVERYBEAM_COMMIT} \
                    --build-arg DP3_COMMIT=${DP3_COMMIT} \
                    -t $1

