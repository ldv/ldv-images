#!/bin/bash

fetch_latest_tag () {
    git -c 'versionsort.suffix=-' \
    ls-remote --exit-code --refs --sort='v:refname' --tags $1 "[0-9].*.[0-9]" "[0-9].[0-9]" "v[0-9].[0-9]" "v[0-9].[0-9].[0-9]" \
    | tail --lines=1 \
    | cut --delimiter='/' --fields=3
}


fetch_latest_commit () {
    echo "$(git ls-remote $1 HEAD | awk '{ print $1 }')"
}

LOFARSTMAN_COMMIT=$(fetch_latest_commit https://github.com/lofar-astron/LofarStMan)
DYSCO_COMMIT=$(fetch_latest_tag https://github.com/aroffringa/dysco.git)
IDG_COMMIT=$(fetch_latest_tag https://git.astron.nl/RD/idg.git)
AOFLAGGER_COMMIT=$(fetch_latest_tag https://gitlab.com/aroffringa/aoflagger.git)
SAGECAL_COMMIT=$(fetch_latest_tag https://github.com/nlesc-dirac/sagecal)
LOFARBEAM_COMMIT=$(fetch_latest_tag https://github.com/lofar-astron/LOFARBeam.git)
EVERYBEAM_COMMIT=$(fetch_latest_tag https://git.astron.nl/RD/EveryBeam.git)
DP3_COMMIT=$(fetch_latest_tag https://git.astron.nl/RD/DP3.git)
WSCLEAN_COMMIT=$(fetch_latest_tag https://gitlab.com/aroffringa/wsclean.git)
LSMTOOL_COMMIT=$(fetch_latest_tag https://git.astron.nl/RD/LSMTool.git)


echo LOFARSTMAN_COMMIT=${LOFARSTMAN_COMMIT}
echo DYSCO_COMMIT=${DYSCO_COMMIT}
echo IDG_COMMIT=${IDG_COMMIT}
echo AOFLAGGER_COMMIT=${AOFLAGGER_COMMIT}
echo SAGECAL_COMMIT=${SAGECAL_COMMIT}
echo LOFARBEAM_COMMIT=${LOFARBEAM_COMMIT}
echo EVERYBEAM_COMMIT=${EVERYBEAM_COMMIT}
echo DP3_COMMIT=${DP3_COMMIT}
echo WSCLEAN_COMMIT=${WSCLEAN_COMMIT}
echo LSMTOOL_COMMIT=${LSMTOOL_COMMIT}
