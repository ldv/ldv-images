#!/usr/bin/env python3
import json
from argparse import ArgumentParser
from datetime import datetime
from datetime import timezone as tz

from astropy.time import Time
from casacore.tables import table as MSTable

_DAY_IN_SECONDS = 24 * 60 * 60

FIXABLE_TIME_RANGES= {
    'FIX_ANTENNA_TABLE': (
        datetime(year=2013, month=2, day=13),
        datetime(year=2014, month=2, day=10)),
    'FIX_WEIGHT_SPECTRUM': (
        datetime(year=2014, month=3, day=19),
        datetime(year=2014, month=10, day=31)),
    'FIX_BROKEN_TILES': (
        datetime(year=2015, month=1, day=26),
        datetime(year=2015, month=2, day=25)),
    'FIX_STATION_ADDER': (
        datetime(year=1999, month=1, day=26),
        datetime(year=2015, month=7, day=28)),
}

UNFIXABLE_TIME_RANGES_LBA= {
    'INACCURATE_FLAGGING_LBA': (
        datetime(year=2012, month=10, day=19),
        datetime(year=2016, month=1, day=25)),
    'FAULTY_LBA_CALIBRATION_TABLES': (
        datetime(year=2015, month=9, day=21),
        datetime(year=2016, month=11, day=22))
}

UNFIXABLE_TIME_RANGES_ALL= {
    'STATION_SENSITIVITY_ISSUE': (
        datetime(year=2015, month=8,  day=31),
        datetime(year=2016, month=6,  day=7)),
    'DELAY_COMPENSATION_ISSUE': (
        datetime(year=2014, month=4, day=14),
        datetime(year=2016, month=9, day=13))
}

_storage_writer_to_storage_manager = {
    'DyscoStMan': 'DyscoStorageManager',
    'TiledColumnStMan': 'CasaStorageManager',
    'LofarStMan': 'LOFARStorageManager'
}

def parse_args():
    parser = ArgumentParser(description='Fix common LOFAR observing issues')
    parser.add_argument('msin', help='Measurement Set input')
    parser.add_argument('--is_long_baseline', help='Is the MeasurementSet going to be used for a long baseline pipeline?')
    return parser.parse_args()

def date_to_fix_string(obs_date, is_long_baseline=False, issues=None):
    if issues is None:
        issues = FIXABLE_TIME_RANGES
    fixes = []
    for fix_string, (start_date, end_date) in issues.items():
        if not is_long_baseline and fix_string == 'FIX_STATION_ADDER':
            continue
        if start_date < obs_date < end_date:
            fixes.append(fix_string)
    return fixes

def check_observing_date(msin):
    observation_table = MSTable(f'{msin}/OBSERVATION', ack=False)
    start_mjd, end_mjd = observation_table.getcell('TIME_RANGE', 0)
    start_datetime, end_datetime = map(mjd_to_local, (start_mjd, end_mjd))
    return start_datetime, end_datetime

def is_raw(msin):
    data_table = MSTable(msin, ack=False)
    return data_table.getdminfo('DATA')['TYPE'] == 'LofarStMan'

def is_lba(msin):
    data_table = MSTable(f'{msin}/OBSERVATION', ack=False)
    return data_table.getcell('LOFAR_FILTER_SELECTION', 0).startswith('LBA')

def mjd_to_local(mjd):
    return Time(mjd / _DAY_IN_SECONDS, format='mjd').to_datetime()

def has_sun_target(msin):
    observation_table = MSTable(f"{msin}/OBSERVATION", ack=False)
    targets = observation_table.getcol("LOFAR_TARGET")["array"]
    for name in targets:
        if "sun" in name.lower():
            return True
    return False

def get_storage_manager(msin):
    st_writer = MSTable(msin, ack=False).getdminfo("DATA")["TYPE"]
    return _storage_writer_to_storage_manager[st_writer]

def main():
    args = parse_args()
    start_datetime_obs, _ = check_observing_date(args.msin)

    sun_target = has_sun_target(args.msin)
    if sun_target == True:
        raise Exception("Targeting the Sun, not suitable for compression.")

    result = {
        'fixable_issues': date_to_fix_string(start_datetime_obs, args.is_long_baseline),
        'is_raw': is_raw(args.msin),
        'unfixable_issues': date_to_fix_string(obs_date=start_datetime_obs,
                                               is_long_baseline=args.is_long_baseline,
                                               issues=UNFIXABLE_TIME_RANGES_ALL),
        'skip_compress': sun_target,
        'input_storage_manager': get_storage_manager(args.msin)
    }

    if is_lba(args.msin):
        result['unfixable_issues'] += date_to_fix_string(obs_date=start_datetime_obs,
                                                         is_long_baseline=args.is_long_baseline,
                                                         issues=UNFIXABLE_TIME_RANGES_LBA)

    print(json.dumps(result, indent=3))
if __name__ == '__main__':
    main()
