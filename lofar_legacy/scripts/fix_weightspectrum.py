#!/usr/bin/env python3
from argparse import ArgumentParser
from casacore.tables import table as mstable, taql
import numpy

def parse_args():
    parser = ArgumentParser(description='Fix weight spectrum')
    parser.add_argument('msin', help='Input measurement set')
    return parser.parse_args()


def check_autoweight(msin):
    history_table = mstable(f'{msin}/HISTORY')
    dppp_history = history_table.query('APPLICATION=\'DPPP\' or APPLICATION=\'NDPPP\'')
    
    for row_index in range(dppp_history.nrows()):
        app_params = dppp_history.getcell('APP_PARAMS', row_index)
        is_autoweight_in_line = any(('AUTOWEIGHT=TRUE' in line.upper() 
                                    for line in 
                                        app_params))
        if is_autoweight_in_line:
            return True
    return False    
    
def derive_averaging_parameters(msin):
    history_table = mstable(f'{msin}/HISTORY')
    dppp_history = history_table.query('APPLICATION=\'DPPP\' or APPLICATION=\'NDPPP\'')
    freqsteps_in_history = []
    timesteps_in_history = []
    for row_index in range(dppp_history.nrows()):
        app_params = dppp_history.getcell('APP_PARAMS', row_index)
        steps = None
        for line in app_params:
            if 'steps' in line:
                steps = line
        
        for line in app_params:
            
            key, value = line.split('=')
            if '.' not in key:
                continue
            step_name, param_name = key.split('.', 1)
            
            if 'FREQSTEP' in param_name.upper() and step_name in steps:
                freqsteps_in_history.append(int(value))
            if 'TIMESTEP' in param_name.upper() and step_name in steps:
                timesteps_in_history.append(int(value))
    
    return numpy.product(timesteps_in_history), numpy.product(freqsteps_in_history)
    
    
def apply_correction_not_autoweight(msin, t_steps, f_steps):
    mult = 1
    if t_steps:
        mult *= t_steps
    if f_steps:
        mult *= f_steps
        
    taql('update $msin set FLAG=True where any(WEIGHT_SPECTRUM > $mult) and ANTENNA1!=ANTENNA2')
    

def apply_correction_is_autoweight(msin):
    apply_correction_not_autoweight(msin, None, None)
    
def update_history_table(msin):
    t = taql ('insert into "%s/HISTORY" (TIME,OBSERVATION_ID,MESSAGE,PRIORITY,ORIGIN,APPLICATION,CLI_COMMAND,APP_PARAMS) values (mjd(),0,"Fixed MS weight","NORMAL","fix_weightspectrum","fix_weightspectrum", array([""],0),array([""],0))' % msin)

def main():
    args = parse_args()
    t_steps, f_steps = derive_averaging_parameters(args.msin)
    
    if check_autoweight(args.msin):
        
        print(f'apply correction for autoweight with time step of {t_steps} and freq step of {f_steps}')
        apply_correction_is_autoweight(args.msin)
    else:
        print(f'apply correction for not autoweight with time step of {t_steps} and freq step of {f_steps}')
        apply_correction_not_autoweight(args.msin, t_steps, f_steps)
    update_history_table(args.msin)
    print('correction applied successfully')
    

if __name__ == '__main__':
    main()
