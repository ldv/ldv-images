#!/usr/bin/env python3
import pyrap.tables as pt
import numpy as np
import argparse
import os

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('-f', dest='vis', help="msfile", required=True)
    parser.add_argument('-c', dest='column', default='DATA', type=str, help="column (default DATA)")
    parser.add_argument('-s', dest='combinedstations', default='CS', type=str, help="combined stations (default CS)")
    parser.add_argument('-n', dest='newstation', default='ST001', help="new station name (default ST001)")

    args = parser.parse_args()
    vis = args.vis
    column = args.column
    combinedstations = args.combinedstations
    newstation = args.newstation

    ## get antenna information
    ant_table = pt.table(vis+'/ANTENNA', readonly=True)
    ant_names = ant_table.getcol('NAME')
    lofar_ids = range(len(ant_names))
    ## new station antenna id    
    ant_id = [ lofid for lofid in lofar_ids if newstation in ant_names[lofid] ][0]
    ## combined stations ids
    cs_ind = [ lofid for lofid in lofar_ids if combinedstations in ant_names[lofid] ]
    nants = np.max(cs_ind) + 1
    ## non-combined antennas
    cs_ind.append(ant_id)
    other_station_ids = [ i for i in lofar_ids if i not in cs_ind ]
    cs_ind.remove(ant_id)

    ## get visibility data
    vis_table = pt.table(vis,readonly=False)
    vis_times = vis_table.getcol('TIME')
    vis_data = vis_table.getcol(column)
    vis_ant1 = vis_table.getcol('ANTENNA1')
    vis_ant2 = vis_table.getcol('ANTENNA2')
    vis_weights = vis_table.getcol('WEIGHT_SPECTRUM')
    vis_flags = vis_table.getcol('FLAG')
    vis_uvw = vis_table.getcol('UVW')

    ## general information
    ntimeslots = len(np.unique(vis_times))
    nchans = vis_data.shape[1]
    npols = vis_data.shape[2]

    ## apply flags
    unfl_data = vis_data * ~vis_flags
    unfl_weights = vis_weights * ~vis_flags

    ## mask where the new station is
    newstn_mask = ((vis_ant1 == ant_id)|(vis_ant2 == ant_id)) - ((vis_ant1 == ant_id)&(vis_ant2 == ant_id))

    ## loop through other antennas
    for other_id in other_station_ids:

        print('Fixing antenna ', ant_names[other_id])

        ## mask where the antenna is
        other_mask = ((vis_ant1 == other_id)|(vis_ant2 == other_id)) - ((vis_ant1 == other_id)&(vis_ant2 == other_id))

        ## find the intersection between other antennas and new station
        other_newstn_mask = other_mask * newstn_mask
        ## index for these visibilities
        other_newstn_ind = np.where(other_newstn_mask)[0]

        ## find the intersection between added stations and other station
        other_ind = np.where(other_mask)[0]
        combined_other_ind = [ i for i in other_ind if vis_ant1[i] < nants or vis_ant2[i] < nants ]

        ## get the uncombined visibilities, weights, and uvw
        uncombined_vis_vec = unfl_data[combined_other_ind,:,:]
        uncombined_wt_vec = unfl_weights[combined_other_ind,:,:]
        uncombined_uvw_vec = vis_uvw[combined_other_ind,:]

        ## need to divide the uncombined visibilities and weights into timeslots
        uncombined_vis_reshape = np.reshape(uncombined_vis_vec, (ntimeslots, nants, nchans, npols )) ##
        uncombined_wt_reshape = np.reshape(uncombined_wt_vec, (ntimeslots, nants, nchans, npols )) 
        uncombined_uvw_reshape = np.reshape(uncombined_uvw_vec, (ntimeslots, nants, 3))

        ## sum weights along the antenna axis
        sum_uncombined_wt_reshape = np.sum( uncombined_wt_reshape, axis=1 )

        ## calculate the weighted sum of the visilibities
        weighted_sum_vis = np.sum( uncombined_vis_reshape * uncombined_wt_reshape, axis=1 ) / sum_uncombined_wt_reshape 
        ## replace nans with zeros ##
        weighted_sum_vis[np.isnan(weighted_sum_vis)] = 0j

        ## also fix the u,v,w
        sum_wts = np.sum(np.sum(uncombined_wt_reshape, axis=3), axis=2)
        sum_wts_uvw = np.dstack((sum_wts, sum_wts, sum_wts))
        ## core stations will always be antenna 1, but the new station will always be antenna 2 
        ## so there is a sign flip in the uvw coordinates
        weighted_sum_uvw = -np.sum( uncombined_uvw_reshape * sum_wts_uvw, axis = 1 ) / np.sum ( sum_wts_uvw, axis=1 )
        weighted_sum_uvw[np.isnan(weighted_sum_uvw)] = 0.0

        for i, rownumber in enumerate(other_newstn_ind):
            vis_table.putcell(column, rownumber, weighted_sum_vis[i,:,:])
            vis_table.putcell('UVW', rownumber, weighted_sum_uvw[i,:])

    vis_table.flush()
    vis_table.close()
    
    t = taql ('insert into "%s/HISTORY" (TIME,OBSERVATION_ID,MESSAGE,PRIORITY,ORIGIN,APPLICATION,CLI_COMMAND,APP_PARAMS) values (mjd(),0,"Fixed UVW averaging","NORMAL","fix_weightedsum_uvw","fix_weightedsum_uvw", array([""],0),array([""],0))' % vis)
    

if __name__ == "__main__":
    main()









    


















