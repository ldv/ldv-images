#! /bin/bash
set -e

LOFAR_TAG=master

SCRIPT_PATH=$(realpath ${BASH_SOURCE[0]})
DOCKER_PATH=$(dirname ${SCRIPT_PATH})

docker build ${DOCKER_PATH} --pull --build-arg=LOFAR_TAG=${LOFAR_TAG} \
                            -t $1 
